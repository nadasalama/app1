// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../customer/layouts/end_order.dart';
import '../../customer/layouts/home/Home.dart';
import '../../customer/layouts/inter_code.dart';
import '../../customer/layouts/log_in.dart';
import '../../customer/layouts/new_user.dart';
import '../../customer/layouts/product.dart';
import '../../customer/layouts/splash.dart';

class Routes {
  static const String splash = '/';
  static const String logIn = '/log-in';
  static const String newUser = '/new-user';
  static const String activeAccount = '/active-account';
  static const String home = '/Home';
  static const String product = '/Product';
  static const String endOrder = '/end-order';
  static const all = <String>{
    splash,
    logIn,
    newUser,
    activeAccount,
    home,
    product,
    endOrder,
  };
}

class AppRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.splash, page: Splash),
    RouteDef(Routes.logIn, page: LogIn),
    RouteDef(Routes.newUser, page: NewUser),
    RouteDef(Routes.activeAccount, page: ActiveAccount),
    RouteDef(Routes.home, page: Home),
    RouteDef(Routes.product, page: Product),
    RouteDef(Routes.endOrder, page: EndOrder),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    Splash: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => Splash(),
        settings: data,
      );
    },
    LogIn: (data) {
      return PageRouteBuilder<dynamic>(
        pageBuilder: (context, animation, secondaryAnimation) => LogIn(),
        settings: data,
        transitionsBuilder: TransitionsBuilders.slideRightWithFade,
      );
    },
    NewUser: (data) {
      return PageRouteBuilder<dynamic>(
        pageBuilder: (context, animation, secondaryAnimation) => NewUser(),
        settings: data,
        transitionsBuilder: TransitionsBuilders.slideRightWithFade,
      );
    },
    ActiveAccount: (data) {
      return PageRouteBuilder<dynamic>(
        pageBuilder: (context, animation, secondaryAnimation) =>
            ActiveAccount(),
        settings: data,
        transitionsBuilder: TransitionsBuilders.slideRightWithFade,
      );
    },
    Home: (data) {
      return PageRouteBuilder<dynamic>(
        pageBuilder: (context, animation, secondaryAnimation) => Home(),
        settings: data,
        transitionsBuilder: TransitionsBuilders.slideRightWithFade,
      );
    },
    Product: (data) {
      return PageRouteBuilder<dynamic>(
        pageBuilder: (context, animation, secondaryAnimation) => Product(),
        settings: data,
        transitionsBuilder: TransitionsBuilders.slideRightWithFade,
      );
    },
    EndOrder: (data) {
      return PageRouteBuilder<dynamic>(
        pageBuilder: (context, animation, secondaryAnimation) => EndOrder(),
        settings: data,
        transitionsBuilder: TransitionsBuilders.slideRightWithFade,
      );
    },
  };
}

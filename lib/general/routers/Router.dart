part of 'RouterImports.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    //general routes
    MaterialRoute(
      page: Splash,
      initial: true,
    ),
    CustomRoute(
        page: LogIn,
        transitionsBuilder: TransitionsBuilders.slideRightWithFade),
    CustomRoute(
        page: NewUser,
        transitionsBuilder: TransitionsBuilders.slideRightWithFade),

    CustomRoute(
        page: ActiveAccount,
        transitionsBuilder: TransitionsBuilders.slideRightWithFade),

    CustomRoute(
        page: Home,
        transitionsBuilder: TransitionsBuilders.slideRightWithFade),
    CustomRoute(
        page: Product,
        transitionsBuilder: TransitionsBuilders.slideRightWithFade),
    CustomRoute(
        page: EndOrder,
        transitionsBuilder: TransitionsBuilders.slideRightWithFade),


  ],
)
class $AppRouter {}

import 'package:app1/customer/layouts/end_order.dart';
import 'package:app1/customer/layouts/home/Home.dart';
import 'package:app1/customer/layouts/inter_code.dart';
import 'package:app1/customer/layouts/log_in.dart';
import 'package:app1/customer/layouts/new_user.dart';
import 'package:app1/customer/layouts/product.dart';
import 'package:app1/customer/layouts/splash.dart';
import 'package:auto_route/auto_route.dart';
import 'package:auto_route/auto_route_annotations.dart';

part 'Router.dart';

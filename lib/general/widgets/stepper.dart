import 'package:app1/general/widgets/MyColor.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

Widget buildStepper({
  @required BuildContext context,
  bool step1 = false,
  bool step2 = false,
  bool step3 = false,
}) {
  return Container(
    height: 80,
    width: MediaQuery.of(context).size.width,
    margin: const EdgeInsets.symmetric(vertical: 20),
    child: Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 7.5),
          child: Container(
            height: 35,
            width: MediaQuery.of(context).size.width * 0.8,
            child: Card(
              color: MyColors.white,
              elevation: 10,
            ),
          ),
        ),
        Container(
          height: 85,
          width: MediaQuery.of(context).size.width * 0.8,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      height: 45,
                      width: 50,
                      child: Center(
                        child: Text(
                          "1",
                          style: TextStyle(
                            color: step1 || step2 || step3 == true
                                ? MyColors.white
                                : MyColors.primary,
                          ),
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: step1 || step2 || step3 == true
                              ? MyColors.blackOpacity
                              : MyColors.primary,
                          border: Border.all(color: MyColors.grey),
                          shape: BoxShape.circle),
                    ),
                    Text(
                      "الوقت والعنوان",
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Container(
                      height: 45,
                      width: 50,
                      child: Center(
                        child: Text(
                          "2",
                          style: TextStyle(
                            color: step2 || step3 == true
                                ? MyColors.white
                                : MyColors.primary,
                          ),
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: step2 || step3 == true
                              ? MyColors.blackOpacity
                              : MyColors.primary,
                          border: Border.all(color: MyColors.grey),
                          shape: BoxShape.circle),
                    ),
                    Text(
                      "تأكيد",
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Container(
                      height: 45,
                      width: 50,
                      child: Center(
                        child: Text(
                          "3",
                          style: TextStyle(
                            color: step3 == true
                                ? MyColors.white
                                : MyColors.primary,
                          ),
                        ),
                      ),
                      decoration: BoxDecoration(
                          color:
                              step3 == true ?    MyColors.blackOpacity
                          : MyColors.primary,
                          border: Border.all(color: MyColors.grey),
                          shape: BoxShape.circle),
                    ),
                    Text(
                      "الدفع",
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}

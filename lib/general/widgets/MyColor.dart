import 'dart:ui';
import 'package:flutter/material.dart';

class MyColors {
  static Color secondary = Color(0xffFFF9EA);
  static Color primary =  Color(0xffFFF3D3);
  static Color grey = Colors.grey;
  static Color greyOpacity = Colors.grey.withOpacity(.5);
  static Color greyWhite = Colors.grey.withOpacity(.2);
  static Color black =Colors.black;
  static Color blackOpacity =Color(0xff313133).withOpacity(.7);
  static Color white = Colors.white;
  static Color greenColor = Colors.green;
  static Color notifyColor = Colors.black54;

  static setColors({Color primaryColor, Color secondaryColor}) {
    primary = primaryColor;
    secondary = secondaryColor;
  }
}

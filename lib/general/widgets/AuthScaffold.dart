import 'package:app1/general/widgets/MyColor.dart';
import 'package:app1/res.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';

class AuthScaffold extends StatelessWidget {
  final Widget child;

  AuthScaffold({this.child});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.primary,
      appBar: AppBar(
        backgroundColor: MyColors.primary,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: MyColors.black,
          ),
          onPressed: () => ExtendedNavigator.of(context).pop(),
        ),
        // title: Image.asset('images/mm.jpeg'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20),
        children: [
          Image.asset(
            Res.logo,
            width: 400,
            height: 250,
          ),
          child
        ],
      ),
    );
  }
}

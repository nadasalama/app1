import 'package:app1/general/widgets/MyColor.dart';
import 'package:flutter/material.dart';

class LabelTextField extends StatelessWidget {
  final String text;

  LabelTextField({this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(3),
      height: 45,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40.5),
      ),
      child: TextField(
        decoration: InputDecoration(
          hintText: text,
          hintStyle: TextStyle(color: Colors.black),
          contentPadding: EdgeInsets.symmetric(horizontal: 20),
          filled: true,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide(color: MyColors.primary, width: 2)),
          errorBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: MyColors.grey.withOpacity(.5), width: 1),
              borderRadius: BorderRadius.circular(30)),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
            borderSide: BorderSide(color: Colors.red, width: 2),
          ),
        ),
      ),
    );
  }
}

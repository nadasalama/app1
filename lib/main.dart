//import 'package:app1/layouts/chang_pass.dart';
//import 'package:app1/layouts/end_order.dart';
//import 'package:app1/layouts/end_order2.dart';

import 'package:app1/customer/layouts/splash.dart';
import 'package:bot_toast/bot_toast.dart';

import 'package:app1/general/routers/RouterImports.gr.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'package:auto_route/auto_route.dart';

import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

void main() {
  runApp(EasyLocalization(
    child: MyApp(),
    supportedLocales: [Locale('ar', 'EG'), Locale('en', 'US')],
    path: 'assets/langs',
    fallbackLocale: Locale('ar', 'EG'),
  ));
}

class MyApp extends StatelessWidget {
  final navigatorKey = new GlobalKey<NavigatorState>();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final botToastBuilder = BotToastInit();

    return MaterialApp(
      title: 'elsada',
      home: Splash(),
      // routes: {
      //   'logIn': (context) {
      //     return LogIn();
      //   },
      //   // 'menu': (context) {
      //   //   return Menu();
      //   // },
      //   // 'madfon': (context) {
      //   //   return Dish();
      //   // },
      //   // 'confirm': (context) {
      //   //   return Confirm();
      //   // },
      //   // 'backHome': (context) {
      //   //   return Finish();
      //   // },
      //   // 'Drawers': (context) {
      //   //   return BuildDrawer();
      //   // },
      //   'back to log in': (context) {
      //     return LogIn();
      //   },
      //   'sign up': (context) {
      //     return NewUser();
      //   },
      //   'log in': (context) {
      //     return LogIn();
      //   },
      //   'inter code': (context) {
      //     return Code();
      //   },
      //   'menu': (context) {
      //     return Menu();
      //   }
      //   // 'home from Drawer': (context) {
      //   //   return Menu();
      //   // },
      //   // 'back to profile': (context) {
      //   //   return Profile();
      //   // },
      //   // 'chang pass': (context) {
      //   //   return ChangPass();
      //   // },
      //   // 'back to drawer': (context) {
      //   //   return BuildDrawer();
      //   // },
      //   // 'shopping basket': (context) {
      //   //   return BuildDrawer();
      //   // },
      //   // 'go to end order': (context) {
      //   //   return EndOrder();
      //   // },
      //   // 'finshing order': (context) {
      //   //   return Finish();
      //   // },
      //   // 'backHom': (context) {
      //   //   return Menu();
      //   // },
      //   // 'confirm': (context) {
      //   //   return EndOrder2();
      //   // },
      // },
      locale: context.locale,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      debugShowCheckedModeBanner: false,
      builder: ExtendedNavigator.builder<AppRouter>(
          router: AppRouter(),
          initialRoute: Routes.splash,
          initialRouteArgs: navigatorKey,
          navigatorKey: navigatorKey,
          builder: (ctx, child) {
            child = FlutterEasyLoading(child: child); //do something
            child = botToastBuilder(context, child);
            return child;
          }),
    );
  }
}

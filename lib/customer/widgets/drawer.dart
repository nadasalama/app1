import 'dart:ui';

import 'package:flutter/material.dart';

class Drawers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
      Drawer(
      child:Container(child:  ListView(children: [
        UserAccountsDrawerHeader(
          accountName: Text('Nada'),
          accountEmail: Text('nadasalama@gmail.com'),
          currentAccountPicture: CircleAvatar(
            child: Icon(Icons.person),
          ),
          decoration: BoxDecoration(
              color: Colors.cyan,
              image: DecorationImage(image: AssetImage('image/images.jpg'))),
        ),
        Divider(
          color: Colors.blueAccent,
        ),
        InkWell(onTap: (){},
            child: ListTile(
              title: Text('Home Page'),
              leading: Icon(Icons.home),

            )),
        Divider(
          color: Colors.blueAccent,
        ),
        InkWell(onTap: (){Navigator.of(context).pushNamed('Categorise');},
            child: ListTile(
              title: Text('Categorise'),
              leading: Icon(Icons.account_balance),)),
        Divider(
          color: Colors.blueAccent,
        ),
        InkWell(onTap: (){},
            child: ListTile(
              title: Text('Siting'),
              leading: Icon(Icons.ac_unit),)),
        Divider(
          color: Colors.blueAccent,
        ),
        ListTile(
          title: Text('Exit'),
          leading: Icon(Icons.exit_to_app),
          onTap: () {},
        )
      ]),)
    );
  }
}

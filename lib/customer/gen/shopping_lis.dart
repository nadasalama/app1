import 'package:flutter/material.dart';

class ShoppingLis extends StatefulWidget {
  @override
  _ShoppingLisState createState() => _ShoppingLisState();
}

class _ShoppingLisState extends State<ShoppingLis> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      margin: const EdgeInsets.all(2),
      padding: const EdgeInsets.all(2),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(2),
          color: Colors.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Text(
                'اسم المنتج',
                style: TextStyle(color: Colors.black, fontSize: 17),
              ),
              Text('19ر.س'),
              Container(
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle, color: Colors.white),
                height: 10,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('1'),
                    IconButton(
                        icon: Icon(Icons.arrow_drop_down_sharp),
                        onPressed: null)
                  ],
                ),
              )
            ],
          ),
          Text(
            '300gm',
            style: TextStyle(color: Colors.black26),
          ),
          Column(
            children: [
              Image.asset(
                'images/mm.jpeg',
                scale: 10,
              ),
              Row(
                children: [
                  Icon(Icons.delete_outlined),
                  Text(
                    'حذف',
                    style: TextStyle(color: Colors.black26),
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}

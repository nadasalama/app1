import 'package:flutter/material.dart';

class ListOrder extends StatefulWidget {
  @override
  _ListOrderState createState() => _ListOrderState();
}

class _ListOrderState extends State<ListOrder> {
  @override
  Widget build(BuildContext context) {
    return Container(height: 100, child: Row(
      children: [
        Column(
          children: [
            Text(
              'اسم المنتج',
              style: TextStyle(
                  color: Colors.black26, fontSize: 15),
            ), Text(
              '19ر.س',
              style: TextStyle(color: Colors.black, fontSize: 14),
            )
          ],
        ),
        Image.asset('images/mm.jpeg', scale: 10,)
      ],
    ),

    );
  }
}

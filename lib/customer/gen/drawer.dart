import 'package:flutter/material.dart';

class BuildDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        height: 400,
        width: 100,
        decoration: BoxDecoration(color: Colors.yellow[100]),
        child: ListView(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 4),
              padding: const EdgeInsets.all(3),
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.rectangle,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  UserAccountsDrawerHeader(
                    accountName: Text('ندى'),
                    accountEmail: Text('966847244135'),
                    currentAccountPicture: Icon(
                      Icons.account_circle,
                    ),
                  ),
                  IconButton(
                      icon: Icon(Icons.assignment_return),
                      onPressed: () {
                        Navigator.of(context).pushNamed("back to log in");
                      })
                ],
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).pushNamed('home from Drawer');
              },
              child: Container(
                margin: const EdgeInsets.only(
                  top: 3,
                ),
                height: 30,
                width: 100,
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(color: Colors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ListTile(
                      title: Text(
                        'الرئيسيه ',
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      ),
                      leading: Icon(
                        Icons.home,
                        size: 7,
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        size: 7,
                        color: Colors.black,
                      ),
                    )
                  ],
                ),
              ),
            ),
            InkWell(onTap: (){},child:
            Container(
              margin: const EdgeInsets.only(
                top: 3,
              ),
              height: 30,
              width: 100,
              padding: const EdgeInsets.all(3),
              decoration: BoxDecoration(color: Colors.white),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ListTile(
                    title: Text(
                      'الملف الشخصى ',
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    ),
                    leading: Icon(
                      Icons.home,
                      size: 7,
                    ),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.perm_identity,
                      size: 7,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
            ),)
            ,InkWell(onTap: (){},child:
            Container(
              margin: const EdgeInsets.only(
                top: 3,
              ),
              height: 30,
              width: 100,
              padding: const EdgeInsets.all(3),
              decoration: BoxDecoration(color: Colors.white),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ListTile(
                    title: Text(
                      'الطلبات ',
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    ),
                    leading: Icon(
                      Icons.reorder,
                      size: 7,
                    ),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      size: 7,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
            ),) ,
            InkWell(onTap: (){},child:
            Container(
              margin: const EdgeInsets.only(
                top: 3,
              ),
              height: 30,
              width: 100,
              padding: const EdgeInsets.all(3),
              decoration: BoxDecoration(color: Colors.white),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ListTile(
                    title: Text(
                      'الاشعارات ',
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    ),
                    leading: Icon(
                      Icons.notifications_none,
                      size: 7,
                    ),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      size: 7,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
            ),)
            ,InkWell(onTap: (){},
              child: Container(
                margin: const EdgeInsets.only(
                  top: 3,
                ),
                height: 30,
                width: 100,
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(color: Colors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ListTile(
                      title: Text(
                        'العناوين ',
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      ),
                      leading: Icon(
                        Icons.notifications_none,
                        size: 7,
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.add_location,
                        size: 7,
                        color: Colors.black,
                      ),
                    )
                  ],
                ),
              ),)
            ,InkWell(onTap: (){},
              child:  Container(
                margin: const EdgeInsets.only(
                  top: 3,
                ),
                height: 30,
                width: 100,
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(color: Colors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ListTile(
                      title: Text(
                        'اللغة ',
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      ),
                      leading: Icon(
                        Icons.language,
                        size: 7,
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        size: 7,
                        color: Colors.black,
                      ),
                    )
                  ],
                ),
              ),)
            ,InkWell(onTap: (){},
              child:   Container(
                margin: const EdgeInsets.only(
                  top: 3,
                ),
                height: 30,
                width: 100,
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(color: Colors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ListTile(
                      title: Text(
                        'الشكاوى والمقترحات  ',
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      ),
                      leading: Icon(
                        Icons.announcement,
                        size: 7,
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        size: 7,
                        color: Colors.black,
                      ),
                    )
                  ],
                ),
              ),)
            ,InkWell(onTap: (){},
              child:  Container(
                margin: const EdgeInsets.only(
                  top: 3,
                ),
                height: 30,
                width: 100,
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(color: Colors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ListTile(
                      title: Text(
                        'الاسئلة المتكرره ',
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      ),
                      leading: Icon(
                        Icons.question_answer,
                        size: 7,
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        size: 7,
                        color: Colors.black,
                      ),
                    )
                  ],
                ),
              ),)
            ,InkWell(onTap: (){},
              child:  Container(
                margin: const EdgeInsets.only(
                  top: 3,
                ),
                height: 30,
                width: 100,
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(color: Colors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ListTile(
                      title: Text(
                        'حول التطبيق',
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      ),
                      leading: Icon(
                        Icons.info_outline,
                        size: 7,
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        size: 7,
                        color: Colors.black,
                      ),
                    )
                  ],
                ),
              ),)
            ,InkWell(onTap: (){},
              child: Container(
                margin: const EdgeInsets.only(
                  top: 3,
                ),
                height: 30,
                width: 100,
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(color: Colors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ListTile(
                      title: Text(
                        'الشروط والاحكام ',
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      ),
                      leading: Icon(
                        Icons.notifications_none,
                        size: 7,
                      ),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.library_books,
                        size: 7,
                        color: Colors.black,
                      ),
                    )
                  ],
                ),
              ),)
            ,
          ],
        ),
      ),
    );
  }
}

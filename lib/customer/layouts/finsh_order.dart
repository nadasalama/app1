import 'package:flutter/material.dart';

class Finish extends StatefulWidget {
  @override
  _FinishState createState() => _FinishState();
}

class _FinishState extends State<Finish> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          backgroundColor: Colors.yellow[100],
          body: Column(
            children: [
              Container(
                height: 40,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    boxShadow: [BoxShadow(color: Colors.black26)]),
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          Icons.arrow_forward_ios,
                          size: 15,
                          color: Colors.black12,
                        ),
                        onPressed: () {}),
                    Text(
                      'اتمام الطلب',
                      style: TextStyle(color: Colors.black, fontSize: 17),
                    )
                  ],
                ),
              ),
              Flexible(
                  child: (Container(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    child: ListView(
                      children: [
                        Container(
                          height: 80,
                          margin: const EdgeInsets.all(5),
                          child: Column(
                            children: [
                              Container(
                                height: 40,
                                width: 100,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      height: 50,
                                      width: 15,
                                      decoration: BoxDecoration(
                                          color: Colors.black,
                                          shape: BoxShape.circle),
                                      child: Icon(
                                        Icons.check,
                                        color: Colors.white,
                                      ),
                                    ),
                                    Container(
                                      height: 50,
                                      width: 15,
                                      decoration: BoxDecoration(
                                          color: Colors.black,
                                          shape: BoxShape.circle),
                                      child: Icon(
                                        Icons.check,
                                        color: Colors.white,
                                      ),
                                    ),
                                    Container(
                                        height: 50,
                                        width: 15,
                                        decoration: BoxDecoration(
                                            color: Colors.black,
                                            shape: BoxShape.circle),
                                        child: Icon(
                                          Icons.check,
                                          color: Colors.white,
                                        ))
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  Text(
                                    ' العنوان',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 15),
                                  ),
                                  Text(
                                    ' تأكيد',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 15),
                                  ),
                                  Text(
                                    ' ارسال الطلب',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 15),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 300,
                          width: MediaQuery.of(context).size.width,
                          padding: const EdgeInsets.all(5),
                          child: Column(
                            children: [
                              IconButton(
                                  icon: Icon(
                                    Icons.check_circle_outline,
                                    size: 25,
                                    color: Colors.black12,
                                  ),
                                  onPressed: () {}),
                              Text(
                                'تم ارسال الطلب بنجاح',
                                style: TextStyle(color: Colors.black, fontSize: 22),
                              ),
                              Text(
                                'سوف يتم الرد عليكم من المطعم',
                                style: TextStyle(color: Colors.black54),
                              ),
                              InkWell(
                                onTap: () {Navigator.of(context).pushNamed('backHome');},
                                child: Container(
                                  margin: const EdgeInsets.only(
                                      top: 20, right: 3, left: 3, bottom: 3),
                                  decoration: BoxDecoration(
                                      color: Colors.black38,
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(7)),
                                  child: Text(
                                    'العودة للرئيسية',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 17),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )))
            ],
          ),
        ));
  }
}

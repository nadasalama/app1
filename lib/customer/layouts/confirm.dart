import 'package:flutter/material.dart';

class Confirm extends StatefulWidget {
  @override
  _ConfirmState createState() => _ConfirmState();
}

class _ConfirmState extends State<Confirm> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
            backgroundColor: Colors.yellow[100],
            body: Column(children: [
              Container(
                decoration: BoxDecoration(
                    boxShadow: [BoxShadow(color: Colors.black12)]),
                padding: const EdgeInsets.all(5),
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          Icons.arrow_forward_ios,
                          size: 15,
                          color: Colors.black12,
                        ),
                        onPressed: () {}),
                    Text(
                      ('اتمام الطلب'),
                      style: TextStyle(color: Colors.black12, fontSize: 22),
                    )
                  ],
                ),
              ),
              Flexible(
                  child: Container(
                    height: 600,
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    child: ListView(
                      children: [
                        Container(
                          height: 80,
                          margin: const EdgeInsets.all(5),
                          child: Column(
                            children: [
                              Container(
                                height: 40,
                                width: 100,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceBetween,
                                  children: [
                                    Container(
                                      height: 50,
                                      width: 15,
                                      decoration: BoxDecoration(
                                          color: Colors.black,
                                          shape: BoxShape.circle),
                                      child: Icon(
                                        Icons.check,
                                        color: Colors.white,
                                      ),
                                    ),
                                    Container(
                                      height: 50,
                                      width: 15,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          shape: BoxShape.circle),
                                      child: Icon(
                                        Icons.check,
                                        color: Colors.white,
                                      ),
                                    ),
                                    Container(
                                        height: 50,
                                        width: 15,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            shape: BoxShape.circle),
                                        child: Text(
                                          '3',
                                          style: TextStyle(
                                              fontSize: 4, color: Colors.black),
                                        ))
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  Text(
                                    ' العنوان',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 15),
                                  ),
                                  Text(
                                    ' تأكيد',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 15),
                                  ),
                                  Text(
                                    ' ارسال الطلب',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 15),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 250,
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          margin: const EdgeInsets.all(5),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    'الاجمالى:',
                                    style: TextStyle(
                                        color: Colors.grey[300], fontSize: 15),
                                  ),
                                  Text(
                                    '0.00ر.س',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 15),
                                  ),
                                  Text(
                                    '(شامل ضريبه القيمه المضافه)',
                                    style: TextStyle(color: Colors.grey),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                    'رسوم التوصيل:',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                  Text(
                                    '0.00ر.س',
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                  )
                                ],
                              ),
                              Text(
                                'الطلب',
                                style: TextStyle(
                                    color: Colors.grey, fontSize: 17),
                              ),
                              Row(
                                children: [
                                  Column(
                                    children: [
                                      Text(
                                        'اسم المنتج',
                                        style: TextStyle(
                                            color: Colors.black45,
                                            fontSize: 17),
                                      ),
                                      Text(
                                        '19ر.س',
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 17),
                                      )
                                    ],
                                  ),
                                  Container(
                                    height: 70,
                                    width: 50,
                                    child: Image.asset(
                                      'images/Fardia.jpeg',
                                      fit: BoxFit.fill,
                                    ),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Column(
                                    children: [
                                      Text(
                                        'اسم المنتج',
                                        style: TextStyle(
                                            color: Colors.black45,
                                            fontSize: 17),
                                      ),
                                      Text(
                                        '19ر.س',
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 17),
                                      )
                                    ],
                                  ),
                                  Container(
                                    height: 70,
                                    width: 50,
                                    child: Image.asset(
                                      'images/Fardia.jpeg',
                                      fit: BoxFit.fill,
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 20,
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          color: Colors.white,
                          decoration: BoxDecoration(shape: BoxShape.rectangle),
                          child: Text(
                            'ملاحظات',
                            style: TextStyle(color: Colors.black, fontSize: 17),
                          ),
                        ),
                        Container(
                          height: 50,
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          color: Colors.white,
                          decoration: BoxDecoration(shape: BoxShape.rectangle),
                          child: Text(
                            'اكتب ملاحظاتك',
                            style: TextStyle(color: Colors.black26),
                          ),
                        ), InkWell(onTap: () {}, child: Container(
                          height: 20,
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          margin: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.black26,
                              shape: BoxShape.rectangle),
                          child: Text(
                            'ستمرار',
                            style: TextStyle(color: Colors.white, fontSize: 17),
                          ),
                        ), )

                      ],
                    ),
                  ))
            ])));
  }
}

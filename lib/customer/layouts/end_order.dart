import 'package:app1/customer/gen/list_order.dart';
import 'package:app1/customer/layouts/end_order/widgets/BuildItem.dart';
import 'package:app1/general/widgets/MyColor.dart';
import 'package:app1/general/widgets/stepper.dart';
import 'package:app1/res.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';

class EndOrder extends StatefulWidget {
  @override
  _EndOrderState createState() => _EndOrderState();
}

class _EndOrderState extends State<EndOrder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.primary,
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: MyColors.primary,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: 25,
            color: MyColors.blackOpacity,
          ),
          onPressed: () => ExtendedNavigator.of(context).pop(),
        ),
        title: Text(
          ('اتمام الطلب'),
          style: TextStyle(color: MyColors.blackOpacity, fontSize: 22),
        ),
      ),
      body: ListView(
        children: [
          // Container(
          //   height: 80,
          //   margin: const EdgeInsets.all(5),
          //   child: Column(
          //     children: [
          //       Container(
          //         height: 30,
          //         width: 80,
          //         decoration: BoxDecoration(
          //             color: Colors.white,
          //             shape: BoxShape.rectangle,
          //             borderRadius: BorderRadius.circular(15)),
          //         child: Row(
          //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //           children: [
          //             Container(
          //               height: 40,
          //               width: 15,
          //               decoration: BoxDecoration(
          //                 color: Colors.black,
          //                 shape: BoxShape.circle,
          //               ),
          //               child: Icon(
          //                 Icons.check,
          //                 color: Colors.white,
          //               ),
          //             ),
          //             Container(
          //                 height: 40,
          //                 width: 15,
          //                 decoration: BoxDecoration(
          //                     color: Colors.white,
          //                     shape: BoxShape.circle),
          //                 child: Text(
          //                   '2',
          //                   style: TextStyle(
          //                       fontSize: 4, color: Colors.black),
          //                 )),
          //             Container(
          //                 height: 40,
          //                 width: 15,
          //                 decoration: BoxDecoration(
          //                     color: Colors.white,
          //                     shape: BoxShape.circle),
          //                 child: Text(
          //                   '3',
          //                   style: TextStyle(
          //                       fontSize: 4, color: Colors.black),
          //                 ))
          //           ],
          //         ),
          //       ),
          //       Row(
          //         children: [
          //           Text(
          //             ' العنوان',
          //             style: TextStyle(color: Colors.black, fontSize: 15),
          //           ),
          //           Text(
          //             ' تأكيد',
          //             style: TextStyle(color: Colors.black, fontSize: 15),
          //           ),
          //           Text(
          //             ' ارسال الطلب',
          //             style: TextStyle(color: Colors.black, fontSize: 15),
          //           )
          //         ],
          //       )
          //     ],
          //   ),
          // ),
          buildStepper(
              context: context, step1: true, step2: false, step3: false),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                BuildItem(
                  title: 'خدمه التوصيل',
                  iconData: Icons.airport_shuttle_outlined,
                  onTap: () {},
                  color: false,
                ),
                BuildItem(
                  title: 'استلام من الفرع',
                  iconData: Icons.airport_shuttle_outlined,
                  onTap: () {},
                  color: true,
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            margin: const EdgeInsets.symmetric(horizontal: 20),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: MyColors.blackOpacity)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.add_location_outlined,
                            size: 20,
                            color: MyColors.blackOpacity,
                          ),
                          Text(
                            'اسم العنوان',
                            style: TextStyle(
                                color: MyColors.blackOpacity, fontSize: 15),
                          )
                        ],
                      ),
                      Text(
                        ' ش عبد العزيز- الرياض المملكه العربيه السعوديه المملكه العربيه السعوديه',
                        style: TextStyle(color: Colors.black),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 80,
                  width: 80,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      image: AssetImage(Res.map),
                      fit: BoxFit.fill,
                    ),
                  ),
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: 20,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        bottomRight: Radius.circular(10),
                        bottomLeft: Radius.circular(10),
                      ),
                      color: MyColors.blackOpacity,
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      "تعديل",
                      style: TextStyle(
                        fontSize: 8,
                        fontWeight: FontWeight.bold,
                        color: MyColors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10),
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Text(
              'اضافة عنوان جديد',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 15),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'الاجمالى:',
                      style:
                          TextStyle(color: MyColors.blackOpacity, fontSize: 15),
                    ),
                    Text(
                      '0.00ر.س',
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    ),
                    Text(
                      '(شامل ضريبه القيمه المضافه)',
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ),
                Row(
                  children: [
                    Text(
                      'رسوم التوصيل:',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      '0.00ر.س',
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                    new Divider(
                      color: Colors.black26,
                    )
                  ],
                ),
                Divider(
                  color: MyColors.grey,
                  height: 20,
                  thickness: 1,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'الطلب',
                          style: TextStyle(color: Colors.grey, fontSize: 17),
                        ),
                        Text(
                          'اسم المنتج',
                          style: TextStyle(color: Colors.black26, fontSize: 15),
                        ),
                        Text(
                          '19ر.س',
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        )
                      ],
                    ),
                    Image.asset(
                      Res.product,
                      width: 60,
                      height: 60,
                    )
                  ],
                )
              ],
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.of(context).pushNamed('confirm');
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              margin: const EdgeInsets.symmetric(horizontal: 20),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.black45,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Text(
                'استمرار',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

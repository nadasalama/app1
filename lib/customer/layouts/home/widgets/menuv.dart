import 'package:app1/general/routers/RouterImports.gr.dart';
import 'package:app1/res.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';

class MenuV extends StatefulWidget {
  @override
  _MenuVState createState() => _MenuVState();
}

class _MenuVState extends State<MenuV> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => ExtendedNavigator.of(context).push(Routes.product),
      child: Container(
        height: 170,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Res.listProduct),
          ),
          borderRadius: BorderRadius.circular(40),
        ),
        alignment: Alignment.bottomCenter,
      ),
    );
  }
}

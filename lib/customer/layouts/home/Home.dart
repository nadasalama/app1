import 'package:app1/customer/layouts/home/widgets/menu_lis.dart';
import 'package:app1/customer/layouts/home/widgets/menuv.dart';
import 'package:flutter/material.dart';
import 'package:app1/general/widgets/MyColor.dart';

import 'package:app1/res.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.primary,
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 30, left: 10, right: 10, bottom: 20),
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  icon: Icon(
                    Icons.more_vert_outlined,
                    size: 35,
                    color: MyColors.blackOpacity,
                  ),
                  onPressed: () {
                    Navigator.of(context).pushNamed('Drawers');
                  },
                ),
                Image.asset(
                  Res.logo,
                  width: 100,
                  height: 100,
                ),
                IconButton(
                  icon: Icon(
                    Icons.shopping_cart_outlined,
                    size: 35,
                    color: MyColors.blackOpacity,
                  ),
                  onPressed: () {},
                )
              ],
            ),
          ),
          Container(
            height: 100,
            width: MediaQuery.of(context).size.width,
            child: ListView.builder(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              scrollDirection: Axis.horizontal,
              itemCount: 6,
              itemBuilder: (context, index) {
                return MenuList();
              },
            ),
          ),
          Flexible(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: ListView.builder(
                padding: EdgeInsets.symmetric(vertical: 20),
                itemCount: 6,
                itemBuilder: (context, index) {
                  return MenuV();
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}

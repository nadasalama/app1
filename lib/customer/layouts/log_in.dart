import 'package:app1/general/widgets/AuthScaffold.dart';
import 'package:app1/res.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:app1/general/routers/RouterImports.gr.dart';

class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  @override
  Widget build(BuildContext context) {
    return AuthScaffold(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'تسجيل الدخول  ',
            style: TextStyle(color: Colors.black, fontSize: 20),
            textAlign: TextAlign.right,
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Text(
              'هذا النص هو مثال للنص الذى يمكن ان يستبدل فى نفس المساحةلقد تم توليد',
              style: TextStyle(fontSize: 18, color: Colors.black26),
            ),
          ),
          Form(
            child: Column(
              children: [
                Container(
                  height: 45,
                  margin: const EdgeInsets.all(7),
                  child: TextField(
                    decoration: InputDecoration(
                        hintText: 'رقم الجوال',
                        hintStyle: TextStyle(color: Colors.black),
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40.5))),
                  ),
                ),
                Container(
                  height: 45,
                  margin: const EdgeInsets.all(7),
                  child: TextField(
                    decoration: InputDecoration(
                      hintText: 'كلمه المرور',
                      hintStyle: TextStyle(color: Colors.black),
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(40.5),
                      ),
                      suffix: Container(
                        color: Colors.white,
                        padding: EdgeInsets.all(5),
                        child: Icon(Icons.remove_red_eye_rounded),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () => ExtendedNavigator.of(context).push(Routes.home),
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              margin: const EdgeInsets.symmetric(vertical: 15),
              decoration: BoxDecoration(
                color: Colors.black26,
                borderRadius: BorderRadius.circular(40.5),
              ),
              child: Text(
                'تسجيل الدخول ',
                style: TextStyle(color: Colors.white, fontSize: 18),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.only(top: 10),
            child: Text(
              'هل نسيت كلمة المرور',
              style: TextStyle(color: Colors.black),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'ليس لديك حساب؟',
                  style: TextStyle(color: Colors.black26),
                ),
                InkWell(
                  onTap: () =>
                      ExtendedNavigator.of(context).push(Routes.newUser),
                  child: Text(
                    'تسجيل جديد',
                    style: TextStyle(color: Colors.black),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

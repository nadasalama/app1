import 'package:app1/general/routers/RouterImports.gr.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
class BuildHaveAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'لدى حساب بالفعل؟',
            style: TextStyle(color: Colors.black26, fontSize: 15),
          ),
          SizedBox(
            width: 5,
          ),
          InkWell(
            onTap: () =>
                ExtendedNavigator.of(context).push(Routes.logIn),
            child: Text(
              'تسجيل الدخول',
              style: TextStyle(color: Colors.black, fontSize: 15),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:app1/general/widgets/TextField.dart';
import 'package:flutter/material.dart';

class BuildRegisterForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return   Form(
      child: Column(
        children: [
          LabelTextField(
            text: "اسم المستخدم",
          ),
          Container(
            margin: const EdgeInsets.all(3),
            height: 45,
            decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(40.5)),
            child: TextField(
              // obscureText: true,
              decoration: InputDecoration(
                hintMaxLines: 1,
                hintText: 'رقم الجوال',
                hintStyle: TextStyle(color: Colors.black26),
                filled: true,
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(40.5)),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.all(3),
            height: 45,
            decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(40.5)),
            child: TextField(
              obscureText: true,
              decoration: InputDecoration(
                hintText: 'كلمه المرور',
                hintStyle: TextStyle(color: Colors.black26),
                filled: true,
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(40.5)),
                suffix: Icon(Icons.remove_red_eye_rounded),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.all(3),
            height: 45,
            decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(40.5)),
            child: TextField(
              obscureText: true,
              decoration: InputDecoration(
                  hintText: ' تاكيد كلمه المرور',
                  hintStyle: TextStyle(color: Colors.black26),
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(40.5)),
                  suffix: Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(5),
                    child: Icon(Icons.remove_red_eye_rounded),
                  )),
            ),
          ),

        ],
      ),
    );
  }
}

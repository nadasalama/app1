import 'package:app1/general/widgets/MyColor.dart';
import 'package:flutter/material.dart';

class BuildItem extends StatelessWidget {
  final String title;
  final IconData iconData;

  final Function onTap;
  final bool color;

  BuildItem({this.title, this.iconData, this.onTap, this.color = false});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          margin: const EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            color: color ? MyColors.primary : MyColors.blackOpacity,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
                color: color ? MyColors.blackOpacity : MyColors.primary),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                iconData,
                color: color ? MyColors.blackOpacity : MyColors.primary,
                size: 20,
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                title,
                style: TextStyle(
                    color: color ? MyColors.blackOpacity : MyColors.primary,
                    fontSize: 15),
              )
            ],
          ),
        ),
      ),
    );
  }
}

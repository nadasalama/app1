import 'package:app1/customer/gen/list_order.dart';
import 'package:flutter/material.dart';

class Recive extends StatefulWidget {
  @override
  _ReciveState createState() => _ReciveState();
}

class _ReciveState extends State<Recive> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
            backgroundColor: Colors.yellow[100],
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_forward_ios),
              ),
              title: Text(
                'اتمام الطلب',
                style: TextStyle(color: Colors.black, fontSize: 17),
              ),
            ),
            body: Container(
                decoration: BoxDecoration(
                    boxShadow: [BoxShadow(color: Colors.black26)]),
                height: 600,
                width: MediaQuery.of(context).size.width,
                child: ListView(children: [
                  Container(
                    height: 80,
                    margin: const EdgeInsets.all(5),
                    child: Column(
                      children: [
                        Container(
                          height: 30,
                          width: 80,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(15)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: 40,
                                width: 15,
                                decoration: BoxDecoration(
                                    color: Colors.black,
                                    shape: BoxShape.circle),
                                child: Icon(
                                  Icons.check,
                                  color: Colors.white,
                                ),
                              ),
                              Container(
                                  height: 40,
                                  width: 15,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle),
                                  child: Text(
                                    '2',
                                    style: TextStyle(
                                        fontSize: 4, color: Colors.black),
                                  )),
                              Container(
                                  height: 40,
                                  width: 15,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle),
                                  child: Text(
                                    '3',
                                    style: TextStyle(
                                        fontSize: 4, color: Colors.black),
                                  ))
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            Text(
                              ' العنوان',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 15),
                            ),
                            Text(
                              ' تأكيد',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 15),
                            ),
                            Text(
                              ' ارسال الطلب',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 15),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(3),
                    height: 80,
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(2),
                          height: 20,
                          width: 40,
                          decoration: BoxDecoration(color: Colors.black),
                          child: Row(
                            children: [
                              Icon(
                                Icons.airport_shuttle_outlined,
                                color: Colors.white,
                              ),
                              Text(
                                'خدمه التوصيل',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 15),
                              )
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pushNamed('استلام من الفرع');
                          },
                          child: Container(
                            padding: const EdgeInsets.all(2),
                            height: 20,
                            width: 40,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                borderRadius: BorderRadius.circular(10)),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.home_work_outlined,
                                  color: Colors.white,
                                ),
                                Text(
                                  'استلام من الفرع',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                )
                              ],
                            ),
                          ),
                        ),
                        Container(
                          height: 100,
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.all(3),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5)),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    'الاجمالى:',
                                    style: TextStyle(
                                        color: Colors.grey[300], fontSize: 15),
                                  ),
                                  Text(
                                    '0.00ر.س',
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 15),
                                  ),
                                  Text(
                                    '(شامل ضريبه القيمه المضافه)',
                                    style: TextStyle(color: Colors.grey),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                    'رسوم التوصيل:',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                  Text(
                                    '0.00ر.س',
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                  ),
                                  new Divider(
                                    color: Colors.black26,
                                  )
                                ],
                              ),
                              Text(
                                'الطلب',
                                style: TextStyle(color: Colors.grey, fontSize: 17),
                              ),
                              ListView.builder(
                                  itemCount: 1,
                                  itemBuilder: (context, index) {
                                    return ListOrder();
                                  })
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),  InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed('confirm');
                    },
                    child: Container(
                      margin: const EdgeInsets.only(
                          bottom: 5, left: 5, right: 5, top: 15),
                      decoration: BoxDecoration(color: Colors.black45),
                      child: Text(
                        'استمرار',
                        style: TextStyle(color: Colors.white, fontSize: 17),
                      ),
                    ),
                  )
                ]))));
  }
}

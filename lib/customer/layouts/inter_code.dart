import 'package:app1/res.dart';
import 'package:flutter/material.dart';

class ActiveAccount extends StatefulWidget {
  @override
  _ActiveAccountState createState() => _ActiveAccountState();
}

class _ActiveAccountState extends State<ActiveAccount> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Scaffold(
              backgroundColor: Color(0xffFFF3D3),
              appBar: AppBar(
                  leading: Icon(Icons.arrow_forward_ios_outlined),
                  backgroundColor: Color(0xffFFF3D3)),
              body: ListView(
                children: [
                  Container(
                    child: Image.asset(Res.logo),
                  ),
                ],
              )),
        ));
  }
}

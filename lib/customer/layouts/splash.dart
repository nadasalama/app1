import 'dart:async';
import 'package:auto_route/auto_route.dart';
import 'package:app1/general/routers/RouterImports.gr.dart';
import 'package:app1/res.dart';
import 'package:flutter/material.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  void initState() {
    Timer(Duration(seconds: 3),
        () => ExtendedNavigator.of(context).push(Routes.newUser));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffFFF3D3),
      body: Center(
        child: Image.asset(Res.logo),
      ),
    );
  }
}

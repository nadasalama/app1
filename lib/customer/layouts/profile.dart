import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          backgroundColor: Colors.yellow[100],
          appBar: AppBar(
            actions: [
              IconButton(
                  icon: Icon(Icons.shopping_cart_outlined), onPressed: () {
                    Navigator.of(context).pushNamed('shopping basket');
              }),
            ],
            leading: IconButton(
              icon: Icon(
                Icons.arrow_forward_ios_outlined,
              ),
              onPressed: () {},
            ),
            title: Text(
              'الملف الشخصي',
              style: TextStyle(
                color: Colors.black,
                fontSize: 17,
              ),
            ),
          ),
          body: ListView(
            children: [
              Container(
                padding: const EdgeInsets.all(3),
                margin:
                    const EdgeInsets.only(top: 5, bottom: 5, left: 7, right: 7),
                decoration: BoxDecoration(shape: BoxShape.circle),
                child: Image.asset(
                  'images/mm.jpeg',
                  scale: 10,
                ),
              ),
              Column(
                children: [
                  Text(
                    'الاسم',
                    style: TextStyle(color: Colors.black, fontSize: 13),
                  ),
                  Container(
                    height: 30,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(3),
                        boxShadow: [BoxShadow(color: Colors.black26)]),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [Text('ندى'), Icon(Icons.edit)],
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Text(
                    'رقم الجوال',
                    style: TextStyle(color: Colors.black, fontSize: 13),
                  ),
                  Container(
                    height: 30,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(3),
                        boxShadow: [BoxShadow(color: Colors.black26)]),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [Text("5240145745"), Icon(Icons.edit)],
                    ),
                  )
                ],
              ),
              InkWell(
                onTap: () {Navigator.of(context).pushNamed('chang pass');},
                child: Container(
                  color: Colors.black38,
                  margin: const EdgeInsets.only(top: 10, left: 3, right: 3),
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(3),
                      boxShadow: [BoxShadow(color: Colors.black26)]),
                  child: Text(
                    'تغير كلمه المرور',
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              ),
              InkWell(
                onTap: () {Navigator.of(context).pushNamed('bach to drawer');},
                child: Container(
                  color: Colors.white,
                  margin: const EdgeInsets.only(top: 10, left: 3, right: 3),
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(3),
                      boxShadow: [BoxShadow(color: Colors.black26)]),
                  child: Text(
                    'حفظ التعديلات',
                    style: TextStyle(color: Colors.black, fontSize: 20),
                  ),
                ),
              )
            ],
          ),
        ));
  }
}

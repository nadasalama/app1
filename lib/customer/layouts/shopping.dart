import 'package:app1/customer/gen/shopping_lis.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Shopping extends StatefulWidget {
  @override
  _ShoppingState createState() => _ShoppingState();
}

class _ShoppingState extends State<Shopping> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          bottomNavigationBar: InkWell(
            onTap: () {Navigator.of(context).pushNamed('go to end order');},
            child:
            Container(
              height: 15,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle, color: Colors.black38),
              child: Text(
                'تمام الطلب',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          backgroundColor: Colors.yellow[100],
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(
                Icons.arrow_forward_ios,
              ),
            ),
            title: Text(
              'سله التسوق',
              style: TextStyle(color: Colors.black, fontSize: 17),
            ),
          ),
          body: Container(
              height: 400,
              width: MediaQuery.of(context).size.width,
              decoration:
                  BoxDecoration(boxShadow: [BoxShadow(color: Colors.black26)]),
              child: Column(
                children: [
                  Text(
                    '3منتج',
                    style: TextStyle(color: Colors.black, fontSize: 13),
                  ),
                  Flexible(
                      child: ListView.builder(
                          itemCount: 3,
                          itemBuilder: (context, index) {
                            return ShoppingLis();
                          })),
                  Row(
                    children: [
                      Text(
                        'المبلغ الاجمالى',
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      ),
                      Text(
                        '(شامل ضريبه القيمه المضافه)',
                        style: TextStyle(color: Colors.black26, fontSize: 13),
                      ),
                      Text(
                        '199ر.س',
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      )
                    ],
                  )
                ],
              )),
        ));
  }
}

import 'package:app1/customer/layouts/Register/widgets/BuildHaveAccount.dart';
import 'package:app1/customer/layouts/Register/widgets/BuildRegisterForm.dart';
import 'package:app1/general/routers/RouterImports.gr.dart';
import 'package:app1/general/widgets/AuthScaffold.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';

class NewUser extends StatefulWidget {
  @override
  _NewUserState createState() => _NewUserState();
}

class _NewUserState extends State<NewUser> {
  // bool valuefirst = false;

  @override
  Widget build(BuildContext context) {
    return AuthScaffold(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'تسجيل جديد',
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 15),
            child: Text(
              'هذا النص هو مثال للنص يمكن ان يستبدل فى نفس المساحه لقد تم التوليد',
              style: TextStyle(color: Colors.black26, fontSize: 17),
            ),
          ),
          BuildRegisterForm(),
          Container(
            margin: const EdgeInsets.only(top: 4),
            child: Row(
              children: [
                Icon(Icons.check_box_outline_blank),
                // Checkbox(
                //   checkColor:MyColors.black,
                //   activeColor: MyColors.grey,
                //   value: this.valuefirst,
                //   onChanged: (bool value) {
                //     setState(() {
                //       this.valuefirst = value;
                //     });
                //   },
                // ),
                Text(
                  'قرات واوافق على',
                  style: TextStyle(color: Colors.black26),
                  // textAlign: TextAlign.center,
                ),
                InkWell(
                  onTap: () {},
                  child: Text(
                    'الشروط والاحكام',
                    style: TextStyle(color: Colors.black),
                    //textAlign: TextAlign.start
                  ),
                )
              ],
            ),
          ),
          InkWell(
            onTap: () =>
                ExtendedNavigator.of(context).push(Routes.activeAccount),
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.symmetric(
                  horizontal: 10, vertical: 10),
              margin: const EdgeInsets.symmetric(vertical: 15),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40.5),
                  color: Colors.black45),
              child: Text(
                'استمرار',
                style: TextStyle(color: Colors.white, fontSize: 18),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          BuildHaveAccount(),
        ],
      ),
    );
  }
}

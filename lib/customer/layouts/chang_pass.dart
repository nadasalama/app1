import 'package:flutter/material.dart';

class ChangPass extends StatefulWidget {
  @override
  _ChangPassState createState() => _ChangPassState();
}

class _ChangPassState extends State<ChangPass> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_forward_ios),
              onPressed: () {
                Navigator.of(context).pushNamed('back to profile');
              },
            ),
            title: Text(
              'الملف الشخصى',
              style: TextStyle(color: Colors.black, fontSize: 20),
            ),
            actions: [
              IconButton(
                  icon: Icon(
                    Icons.shopping_cart_outlined,
                    size: 7,
                  ),
                  onPressed: () {})
            ],
          ),
          body: Container(
            height: 300,
            width: MediaQuery.of(context).size.width,
            child: ListView(
              children: [
                Container(
                  margin: const EdgeInsets.all(3),
                  color: Colors.white,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(3)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'كلمة المرور القديمه',
                        style: TextStyle(color: Colors.black26),
                      ),
                      IconButton(
                          icon: Icon(Icons.remove_red_eye_outlined),
                          onPressed: null)
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(3),
                  color: Colors.white,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(3)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'كلمة المرور الجديده',
                        style: TextStyle(color: Colors.black26),
                      ),
                      IconButton(
                          icon: Icon(Icons.remove_red_eye_outlined),
                          onPressed: null)
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(3),
                  color: Colors.white,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(3)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        ' تاكيد كلمة المرور ',
                        style: TextStyle(color: Colors.black26),
                      ),
                      IconButton(
                          icon: Icon(Icons.remove_red_eye_outlined),
                          onPressed: null)
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 10, left: 3, right: 3),
                  color: Colors.black26,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(3)),
                  child: Text(
                    'حفظ التعديلات',
                    style: TextStyle(color: Colors.white),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}

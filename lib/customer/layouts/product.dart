import 'package:app1/general/routers/RouterImports.gr.dart';
import 'package:app1/general/widgets/MyColor.dart';
import 'package:app1/res.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';

class Product extends StatefulWidget {
  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.primary,
      bottomNavigationBar: InkWell(
      onTap: ()=>ExtendedNavigator.of(context).push(Routes.endOrder),
        child: Container(
          padding: const EdgeInsets.all(20),
          color: Colors.grey,
          height: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'اضف السلة',
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              Text(
                '30 ر.س',
                style: TextStyle(color: Colors.white, fontSize: 18),
              )
            ],
          ),
        ),
      ),
      body: Column(
        children: [
          Stack(
            children: [
              Container(
                padding: const EdgeInsets.symmetric(vertical: 50),
                color: MyColors.primary,
                child: Image(
                  image: AssetImage(
                    Res.product,
                  ),
                  height: 250,
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.contain,
                ),
              ),
              Positioned(
                top: 30,
                right: 15,
                child: IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    size: 25,
                  ),
                  onPressed: () => ExtendedNavigator.of(context).pop(),
                ),
              ),
              Positioned(
                bottom: 30,
                left: 15,
                child: Row(
                  children: [
                    Text(
                      "5",
                      style: TextStyle(fontSize: 20),
                    ),
                    Icon(
                      Icons.star,
                      color: Colors.yellow[700],
                    )
                  ],
                ),
              ),
            ],
          ),
          Flexible(
            child: Container(
              decoration: BoxDecoration(
                color: MyColors.white,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  topLeft: Radius.circular(30),
                ),
              ),
              child: ListView(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                children: [
                  Text(
                    'مدفون',
                    style: TextStyle(color: Colors.black, fontSize: 20),
                  ),
                  Text(
                    'هذاالنص هو مثال لنص يمكن ان يستبدل فى نفس المساحه .هذا النص هو مثال لنص يمكن ان يستبدل فى نفس المساحه',
                    style: TextStyle(color: Colors.grey, fontSize: 17),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 5),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              border: Border.all(color: MyColors.grey)),
                          child: Text(
                            '300gm',
                            style: TextStyle(
                              fontSize: 13,
                            ),
                          ),
                        ),
                        Text(
                          '19ر.س',
                          style: TextStyle(color: Colors.black, fontSize: 20),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: MyColors.blackOpacity,
                          ),
                          child: Text(
                            '+',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        Text(
                          '1',
                          style: TextStyle(color: Colors.grey, fontSize: 20),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: MyColors.white,
                              border: Border.all(color: MyColors.blackOpacity)),
                          child: Text(
                            '-',
                            style: TextStyle(color: Colors.black),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    margin: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: MyColors.grey.withOpacity(.5),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'الاضافات',
                          style: TextStyle(color: Colors.black, fontSize: 17),
                        ),
                        IconButton(
                          icon: Icon(Icons.arrow_drop_down),
                          onPressed: () {},
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    margin: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: MyColors.grey.withOpacity(.5),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'انواع الخبز',
                          style: TextStyle(color: Colors.black, fontSize: 17),
                        ),
                        IconButton(
                          icon: Icon(Icons.arrow_drop_down),
                          onPressed: () {},
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                    margin: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: MyColors.grey.withOpacity(.2),
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: MyColors.grey)
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              'خبز اسمر',
                              style: TextStyle(color: Colors.black, fontSize: 17),
                            ),
                            Spacer(),
                            Text(
                              '100 ر.س.',
                              style: TextStyle(color: Colors.black, fontSize: 14),
                            ),

                          ],
                        ),
                        Divider(color: MyColors.grey,thickness: 2,
                        height: 30,),
                        Row(
                          children: [
                            Text(
                              'خبز اسمر',
                              style: TextStyle(color: Colors.black, fontSize: 17),
                            ),
                            Spacer(),
                            Text(
                              '100 ر.س.',
                              style: TextStyle(color: Colors.black, fontSize: 14),
                            ),

                          ],
                        ),

                      ],
                    )
                  ),

                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
